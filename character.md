---
category: pcs / npcs
aliases: first name and/or pseudonym
status: alive / dead / special / unknown / immortal
importance: pantheon / protagonist / antagonist / important / background
campaign: campaign name
---

# Character Name

*One line description*

![[character-image.png|250]]

**Affilation**:: Associated Organizations
**Relation**:: Associated Characters
**Location**:: Associated Places
**Race**:: Character's Race/Ancestry
**Rank**:: Character's Rank In Organization (If Applies)
**Aura**:: Color Of Character's Magical Aura (MTG Style)
**Alignment**:: Character's Alignment
**Height**:: xx ft.
**Age**:: xx
**Born**:: dd mmmm yyyy

---

## Description

Character description.

## Stats

**Level x Class**

| STR     | DEX     | CON     | INT     | WIS    | CHA    |
| ------- | ------- | ------- | ------- | ------ | ------ |
| 10 (+0) </br> ==`dice: d20+0`== | 10 (+0) </br> ==`dice: d20+0`== | 10 (+0) </br> ==`dice: d20+0`== | 10 (+0) </br> ==`dice: d20+0`== | 10 (+0) </br> ==`dice: d20+0`== | 10 (+0) </br> ==`dice: d20+0`== |

|                       |                  |
| --------------------- | ---------------- |
| Total Character Level | xx               |
| Proficiency Bonus     | xx               |
| Armor Class           | xx               |
| Hit Points            | xx               |
| Hit Dice              | xdx (Class)      |
| Passive Perception    | xx (10+Wis+Prof) |
| Speed (Walk)          | xx ft.           |
| Save DC               | xx               |
| Attack (Spell)        | xx               |
| Attack (Strength)     | xx               |

|              |                        |
| ------------ | ---------------------- |
| Saving Throw | ==`dice: d20+0`== (+0) |

|              |                        |
| ------------ | ---------------------- |
| Skill (Stat) | ==`dice: d20+0`== (+0) |

|           |     |
| --------- | --- |
| Languages |     |
| Armor     |     |
| Weapons   |     |
| Tools     |     |

---

## Tracker
**Gold** 💰 100GP
- xxgp description of gold-spending event

<label>**HP** ❤️ xx / xx  <progress max="100" value="100"></progress></label>

**Short Rest**
- [ ] Ability / Spell Slot

**Long Rest**
- [ ] Ability / Spell Slot

---

## Spells
0️⃣ [[Cantrip Name]] **limit** (Origin)
1️⃣ [[Spell Name]] **limit** (Origin)
2️⃣ [[Spell Name]] **limit** (Origin)
3️⃣ [[Spell Name]] **limit** (Origin)
4️⃣ [[Spell Name]] **limit** (Origin)
5️⃣ [[Spell Name]] **limit** (Origin)
6️⃣ [[Spell Name]] **limit** (Origin)
7️⃣ [[Spell Name]] **limit** (Origin)
8️⃣ [[Spell Name]] **limit** (Origin)
9️⃣ [[Spell Name]] **limit** (Origin)

---

## Magical Items
[[Item Name]]

---

## Abilities

**Ability Name.** (Limit)
Ability description.

---

## Actions

**Attack Name.** 
Melee/Ranged Weapon Attack: ==`dice: d20+0`== (+0), Reach x ft., One Target
Hit: ==`dice:d0+0`== (1d0+0) Type Damage

**Other Action.** (Limit)
Action Description.

---

## Bonus Actions

**Bonus Action.** (Limit)
Action Description.

---

## Appearances
```dataview
LIST FROM [[Character Name]] WHERE category = "sessions"
SORT number
```
